package figury;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;
import javax.swing.Timer;


public class AnimPanel extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Figura figura;
	// bufor
	static Image image;
	// wykreslacz ekranowy
	static Graphics2D device;
	// wykreslacz bufora
	static Graphics2D buffer;
	int x,y;
	private int delay = 10;

	private Timer timer;
	
	private static int numer = 0;

	public AnimPanel() {
		super();
		setBackground(Color.WHITE);
		timer = new Timer(delay, this);
	}

	public void initialize() {
		int width = getWidth();
		int height = getHeight();

		image = createImage(300, 200);
		buffer = (Graphics2D) image.getGraphics();
		buffer.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		device = (Graphics2D) getGraphics();
		device.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
	}

	Figura addFig() {
		Figura fig = (numer++ % 2 == 0) ? new Kwadrat(delay, getWidth(), getHeight(),false)
				: new Elipsa(delay, getWidth(), getHeight(),false);
		timer.addActionListener(fig);
		Thread watek=new Thread(fig);
		figura=fig;
		watek.start();
		return figura;
	}
	void animate() {
		if (timer.isRunning()) 
		{
			timer.stop();
			
		} else {
			timer.start();
			
		}
	}
	void scaleBuffer(int x_,int y_)
	{
		x=x_;
		y=y_;
		image = createImage(x, y);
		buffer = (Graphics2D) image.getGraphics();
		device = (Graphics2D) getGraphics();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		device.drawImage(image, 0, 0, null);
		buffer.clearRect(0, 0, 1366, 768);
		buffer.setBackground(Color.WHITE);
		
	}

}
