package figury;

import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import javax.swing.JButton;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class AnimatorApp extends JFrame {

	/**
	 * 
	 */
	static int number = 0,framex,framey;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	protected static List<Figura>Lista=new ArrayList<>();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					final AnimatorApp frame = new AnimatorApp();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	
	/**
	 * Create the frame.
	 * @param delay 
	 */
	public AnimatorApp() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0,0,screen.width, screen.height);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(null);
		AnimPanel kanwa = new AnimPanel();
		kanwa.setBounds(0,0,screen.width, screen.height-100);
		contentPane.add(kanwa);
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				kanwa.initialize();
			}
		});
		this.addComponentListener(new ComponentListener(){


			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentResized(ComponentEvent arg0) {
				framex=AnimatorApp.super.getWidth();
				framey=AnimatorApp.super.getHeight();
				kanwa.scaleBuffer(framex, framey);
			}

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub	
			}
		});
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				Lista.add(kanwa.addFig());
			}
		});
		btnAdd.setBounds(screen.width/2-80, screen.height-90, 80, 23);
		contentPane.add(btnAdd);
		
		JButton btnAnimate = new JButton("Animate");
		btnAnimate.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e)
			{
				kanwa.animate();
				if(number%2==0)
				for(int i=0;i<Lista.size();i++)
					Lista.get(i).accesssleep(false);
				else
					for(int i=0;i<Lista.size();i++)
						Lista.get(i).accesssleep(true);	
				number++;
			}		
		});
		btnAnimate.setBounds(screen.width/2+5, screen.height-90, 80, 23);
		contentPane.add(btnAnimate);
	}
	
}
