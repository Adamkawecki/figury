package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

public class Elipsa extends Figura
{
	public Elipsa(int del, double w, double h,boolean k_) 
	{
		super(del, w, h,false,k_);
		shape=new Ellipse2D.Double(0,0,10,10);
		aft = new AffineTransform();
		area=new Area(shape);
	}

}
