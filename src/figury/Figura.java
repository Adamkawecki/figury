/**
 * 
 */
package figury;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 * @author tb
 *
 */
public abstract class Figura implements Runnable, ActionListener {
	Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
	// wspolny bufor
	protected Area area;
	// do wykreslania
	protected Shape shape;
	// przeksztalcenie obiektu
	protected AffineTransform aft;

	// przesuniecie
	private int dx, dy;
	// rozciaganie
	private double sf;
	// kat obrotu
	private double an;
	private int delay;
	private double width;
	private double height;
	private Color clr;
	private boolean k;
	private boolean z;
	private int tempdx=dx;
	private int tempdy=dy;

	protected static final Random rand = new Random();

	public Figura(int del, double w, double h,boolean k_,boolean z_) {
		delay = del;
		width = w;
		height = h;
		k=k_;
		z=z_;

		dx = 1 + rand.nextInt(2);
		dy = 1 + rand.nextInt(2);
		tempdx=dx;
		tempdy=dy;
		sf = 1 + 0.01 * rand.nextDouble();
		an = 0.1 * rand.nextDouble();
		clr = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255));
		// reszta musi być zawarta w realizacji klasy Figure
		// (tworzenie figury i przygotowanie transformacji)

	}
	Thread getThread()
	{
		return Thread.currentThread();
	}
	@Override
	public void run() {
		// przesuniecie na srodek
		aft.translate(100, 100);
		area.transform(aft);
		shape = area;
		while (true) {
			// przygotowanie nastepnego kadru
			
			try {
				synchronized(Thread.currentThread())
				{
				if(!z)
				{
					shape = nextFrame();
				}
				Thread.sleep(delay);
				}
			} catch (InterruptedException e) {
			}
		}
	}
	void accesssleep(boolean y)
	{
		z=y;
	}
	protected Shape nextFrame() {
		// zapamietanie na zmiennej tymczasowej
		// aby nie przeszkadzalo w wykreslaniu
		area = new Area(shape);
		aft = new AffineTransform();
		Rectangle2D bounds = area.getBounds2D();
		double cx = bounds.getCenterX();
		double cy = bounds.getCenterY();
		// odbicie
		if (bounds.getX() <= 50)
		{
			dx = tempdx= -dx;
			aft.translate(30, 0);
		}
		if(bounds.getMaxX() > AnimatorApp.framex-bounds.getWidth())
		{
			dx = tempdx= -dx;
			aft.translate(-30, 0);
		}
		if (bounds.getY() <= 50) 
		{
			dy = tempdy = -dy;
			aft.translate(0, 30);
		}
		if(bounds.getMaxY() > AnimatorApp.framey-bounds.getHeight()-100)
		{
			dy=tempdy=-dy;
			aft.translate(0, -30);
		}
		// zwiekszenie l
		// zwiekszenie lub zmniejszenie
		// konstrukcja przeksztalcenia
		aft.translate(cx, cy);
		if((0<cy-bounds.getHeight()-25 && cy+bounds.getHeight()<height-25)&&(0 < cx-bounds.getWidth()-25 && cx+bounds.getWidth()<width-25))
		{
			if (bounds.getHeight() > 50)
				{
					aft.scale(0.8, 0.8);
					sf=1/sf;
				}
			else if(bounds.getHeight()<5)
				{
					aft.scale(2.5, 2.5);
					sf=1/sf;
				}
			else
				aft.scale(sf, sf);
		}
		if(k)
			aft.rotate(an);
		aft.translate(-cx, -cy);
		aft.translate(dx, dy);
		// przeksztalcenie obiektu
		area.transform(aft);
		return area;
	}
	@Override
	public void actionPerformed(ActionEvent evt) {
		// wypelnienie obiektu
		AnimPanel.buffer.setColor(clr.brighter());
		AnimPanel.buffer.fill(shape);
		// wykreslenie ramki
		AnimPanel.buffer.setColor(clr.darker());
		AnimPanel.buffer.draw(shape);
	}

}
